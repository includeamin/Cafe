


var mysql = require('mysql');
var load = require('./config.js');
var multer = require('multer');
var upload = multer({ dest:'./upload' });
var copyFile = require('quickly-copy-file');
const fs = require('fs');
var mailecode = require('./verification/Mail');


var connection = mysql.createConnection({
    host: load.dbConfig().host,
    user: load.dbConfig().user,
    password: load.dbConfig().password,
    database: load.dbConfig().database
});

module.exports =(app)=>{
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));


    app.post("/user/invite",(req,res)=>{

    
    var phonenumber = req.body.phonenumber;
    var username = req.body.username;
    var code = mailecode.inviteSMS(username,phonenumber);
    res.send(code)
    


    });
}